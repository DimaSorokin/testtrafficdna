<?php

namespace app\src;

class OddNumbers
{
    function getOddNumber(array $srcValues): int{
        $count = 0;
        for ($i = 0; $i < count($srcValues); $i++) {
            for ($j = 0; $j < count($srcValues); $j++) {
                if ($srcValues[$i] == $srcValues[$j])
                    $count++;
            }
            if ($count % 2 != 0)
                return $srcValues[$i];
        }
        return -1;
    }
}
var_dump((new OddNumbers())->getOddNumber([2,5,9,1,5,1,8,2,8]));