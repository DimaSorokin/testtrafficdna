<?php
namespace app\src;

class Palindrome
{
    public static function isPalindrome(string $word) : bool{
        return strrev(mb_strtolower($word)) == mb_strtolower($word);
    }
}
var_dump(Palindrome::isPalindrome('Deleveled'));