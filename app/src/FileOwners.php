<?php
namespace app\src;

class FileOwners
{
    public static function groupByOwners(array $files) : array {
        $groupFiles = [];
        foreach ($files as $key=>$file){
            $groupFiles[$file][] = $key;
        }
        return $groupFiles;
    }
}
$files = [
    "Input.txt" => "Randy",
    "Code.py" => "Stan",
    "Output.txt" => "Randy"
];
var_dump(FileOwners::groupByOwners($files));