<?php

namespace app\src;

class Thesaurus
{
    private $thesaurus;

    function __construct($thesaurus)
    {
        $this->thesaurus = $thesaurus;
    }
    public function getSynonyms(string $word) : string {
        foreach ($this->thesaurus as $key=> $thesaurus) {
            if($word === $key)
                return $this->preparedString($key, $thesaurus);
        }
        return $this->preparedString($word);
    }

    private function preparedString(string $key, array $synonyms = []) : string{
        return json_encode(['word' => $key, 'synonyms' => $synonyms ]);
    }

}
$thesaurus = new Thesaurus([
    "buy" => ["purchase"],
    "big" => ["great", "large"],
]);
var_dump($thesaurus->getSynonyms("big"));
var_dump($thesaurus->getSynonyms("agelast"));
var_dump($thesaurus->getSynonyms("buy"));