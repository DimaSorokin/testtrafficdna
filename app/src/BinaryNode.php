<?php
namespace app\src;

class BinaryNode {

    public $value = null;
    public $left = null;
    public $right = null;

    public function __construct($value = null) {
        $this->value = $value;
    }

    public function invertTree($value) {

        if ($value == null)
            return $value;

        $left = $this->invertTree($value->left);
        $right = $this->invertTree($value->right);

        $value->left = $right;
        $value->right = $left;

        return $value;
    }
}
