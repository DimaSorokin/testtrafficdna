<?php
namespace app\tests;


use app\src\OddNumbers;
use PHPUnit\Framework\TestCase;

class OddNumberTest extends TestCase {

    public function test_odd_number_test()
    {
        $this->assertEquals(true, true);
        $result = (new OddNumbers())->getOddNumber([2,5,9,1,5,1,8,2,8]);
        $this->assertEquals(9,$result);
        $result = (new OddNumbers())->getOddNumber([2,3,4,3,1,4,5,1,4,2,5]);
        $this->assertEquals(4,$result);
    }
}