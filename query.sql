#Task 1

select school_name, student.student_name, student.city, school.city from root.school as school
left join root.student as student on student.school_id = school.school_id
where student.city != 'New York'
and student.city = school.city

#Task 2

select subject.subject_name, count(student.subject_id) as col from root.subject as subject
left join root.student as student on student.subject_id = subject.subject_id
group by subject.subject_name
having count(student.subject_id) > 2